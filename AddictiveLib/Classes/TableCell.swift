//
//  TableCell.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

open class TableCell {
	
	let reuseIdentifier: String
	
	let nibName: String?
	let nibBundle: Bundle?
	
	private var metadata: [String: Any] = [:]
	
	private(set) var height = UITableViewAutomaticDimension
	private(set) var estimatedHeight: CGFloat?
	
	private(set) var selectionStyle: UITableViewCellSelectionStyle?
	private(set) var deselectOnSelection = true
	
	private(set) var callback: (TableCell, UITableViewCell) -> Void = {_,_ in}
	private(set) var selected: (TableCell, IndexPath) -> Void = {_,_ in}
	
	public init(_ reuseIdentifier: String) {
		self.reuseIdentifier = reuseIdentifier
		self.nibName = nil
		self.nibBundle = nil
	}
	
	public init(_ reuseIdentifier: String, nib: String) {
		self.reuseIdentifier = reuseIdentifier
		self.nibName = nib
		self.nibBundle = Bundle(identifier: Globals.BundleID!)
	}
	
	public init(_ reuseIdentifier: String, nib: String, bundle: String) {
		self.reuseIdentifier = reuseIdentifier
		self.nibName = nib
		self.nibBundle = Bundle(identifier: bundle)
	}
	
	@discardableResult
	public func setHeight(_ height: CGFloat) -> TableCell {
		self.height = height
		
		if self.height != UITableViewAutomaticDimension {
			self.estimatedHeight = self.height
		}
		return self
	}
	
	@discardableResult
	public func setHeight(_ height: Int) -> TableCell {
		return self.setHeight(CGFloat(height))
	}
	
	@discardableResult
	public func setEstimatedHeight(_ height: CGFloat) -> TableCell {
		self.estimatedHeight = height
		return self
	}
	
	@discardableResult
	public func setEstimatedHeight(_ height: Int) -> TableCell {
		return self.setEstimatedHeight(CGFloat(height))
	}
	
	@discardableResult
	public func getEstimatedHeight() -> CGFloat {
		guard let estimated = self.estimatedHeight else {
			return 1.0
		}
		return estimated
	}
	
	@discardableResult
	public func setSelectionStyle(_ selection: UITableViewCellSelectionStyle) -> TableCell {
		self.selectionStyle = selection
		return self
	}
	
	@discardableResult
	public func setCallback(_ callback: @escaping (TableCell, UITableViewCell) -> Void) -> TableCell {
		self.callback = callback
		return self
	}
	
	@discardableResult
	public func setSelection(_ callback: @escaping (TableCell, IndexPath) -> Void) -> TableCell {
		self.selected = callback
		return self
	}
	
	@discardableResult
	public func setDeselectOnSelection(_ flag: Bool) -> TableCell {
		self.deselectOnSelection = flag
		return self
	}
	
	@discardableResult
	public func setMetadata(_ key: String, data: Any) -> TableCell {
		self.metadata[key] = data
		return self
	}
	
	public func getMetadata<Result>(_ key: String) -> Result? {
		guard let result = self.metadata[key] as? Result else {
			return nil
		}
		return result
	}
	
}
