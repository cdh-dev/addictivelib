//
//  WebCall.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/14/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import Alamofire
import Unbox
import UnboxedAlamofire

public protocol WebCallChainable {
	
	var child: WebCallChainable? { get set }
	
	func executeFromParent(success: @escaping () -> Void, failure: @escaping (Error) -> Void)
	func didFinishExecutingChild()
	
}

open class WebCall<Result>: WebCallChainable {

	public var parent: WebCallChainable?
	public var child: WebCallChainable?
	var relaySuccessToParent: () -> Void = {}
	var relayFailureToParent: (Error) -> Void = {_ in}
	var failIfChildFails: Bool = true
	
	public private(set) var method: HTTPMethod = .get
	
	public let url: String
	public let call: String
	public private(set) var overridenUrl: String?
	
	public private(set) var parameters: [String: Any] = [:]
	
	public private(set) var callback: (WebCallResult<Result>) -> Void = {_ in}
	public private(set) var result: WebCallResult<Result>?
	
	public init(call: String) {
		self.url = Globals.getUrlBase()
		self.call = call
	}
	
	public init(url: String, call: String) {
		self.url = url
		self.call = call
	}
	
	public init(base: String, call: String) {
		self.url = Globals.getUrlBase(base)!
		self.call = call
	}
	
	@discardableResult
	public func overrideUrl(url: String) -> WebCall<Result> {
		self.overridenUrl = url
		return self
	}
	
	@discardableResult
	public func setMethod(_ method: HTTPMethod) -> WebCall<Result> {
		self.method = method
		return self
	}
	
	@discardableResult
	public func callback(_ callback: @escaping (WebCallResult<Result>) -> Void) -> WebCall<Result> {
		self.callback = callback
		return self
	}
	
	@discardableResult
	public func parameter(_ key: String, val: Any) -> WebCall<Result> {
		self.parameters[key] = val
		return self
	}
	
	@discardableResult
	public func require(_ webCall: WebCallChainable, failIfChildFails: Bool = true) -> WebCall<Result> {
		self.child = webCall
		self.failIfChildFails = failIfChildFails
		return self
	}
	
	public func executeFromParent(success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
		self.relaySuccessToParent = success
		self.relayFailureToParent = failure
		self.execute()
	}
	
	public func execute() {
		if let child = self.child {
			child.executeFromParent(success: {
				self.didFinishExecutingChild()
				self.doExecute()
			}, failure: {
				childError in
				
				self.didFinishExecutingChild()
				if self.failIfChildFails {
					self.error(error: DownstreamWebFailure(error: childError))
				} else {
					self.doExecute()
				}
			})
		} else {
			self.doExecute()
		}
	}
	
	private func doExecute() {
		let call = self.overridenUrl ?? self.buildCall()
		self.performRequest(request: Alamofire.request(call, method: self.method))
	}
	
	open func didFinishExecutingChild() {
		
	}
	
	open func performRequest(request: DataRequest) {
		request.responseData() {
			response in

			if let error = self.validateResponse(dataResponse: response) {
				self.error(error: error)
				return
			}
			
			guard let result = self.convertData(response.data!) else {
				self.error(error: ResourceNullError(resource: "Converted Data"))
				return
			}
			
			self.success(result: result)
			return
		}
	}
	
	open func validateResponse<DataType>(dataResponse: DataResponse<DataType>) -> Error? {
		if let problem = dataResponse.error {
			return problem
		}
		
		guard let response = dataResponse.response else {
			return ResourceNullError(resource: "HTTPURLResponse")
		}
		
		if response.statusCode < 200 || response.statusCode >= 300 {
			return InvalidWebCodeError(code: response.statusCode)
		}
		
		guard let _ = dataResponse.data else {
			return ResourceNullError(resource: "Response Data")
		}
		
		return nil
	}
	
	open func convertData(_ data: Data) -> Result? {
		return nil
	}
	
	open func handleCall(result: WebCallResult<Result>) {
		// Override point
	}
	
	private func buildCall() -> String {
		var name = "\(self.url)\(self.call)"
		
		if self.parameters.count > 0 {
			name += "?"
			for (key, val) in self.parameters {
				name += "\(key)=\(val)&"
			}
			name = String(name.dropLast())
		}
		return name
	}
	
	func success(result: Result) {
		if ((Globals.getData("debug") as Bool?) ?? false) {
			print("Web call \(self.buildCall()) completed.")
		}
		
		let result: WebCallResult<Result> = WebCallResult.success(result: result)
		self.result = result
		
		self.handleCall(result: result)
		self.callback(result)
		
		self.relaySuccessToParent()
	}
	
	func error(error: Error) {
		if ((Globals.getData("debug") as Bool?) ?? false) {
			print("Web call \(self.buildCall()) failed: \(error.localizedDescription)")
		}
		
		let result: WebCallResult<Result> = WebCallResult.failure(error: error)
		self.result = result
		
		self.handleCall(result: result)
		self.callback(result)
		
		self.relayFailureToParent(error)
	}
	
}

public enum WebCallResult<Payload> {
	
	case success(result: Payload)
	case failure(error: Error)

}

public class InvalidWebCodeError: Error {
	
	public let code: Int
	var localizedDescription: String {
		return "Recieved an invalid response code: \(self.code)"
	}
	
	init(code: Int) {
		self.code = code
	}
	
}

public class DownstreamWebFailure: Error {
	
	public let downstreamError: Error
	
	init(error: Error) {
		self.downstreamError = error
	}
	
	var localizedDescription: String {
		return "A child WebCall failed: \(self.downstreamError.localizedDescription)"
	}
	
}
