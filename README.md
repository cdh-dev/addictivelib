Created by me, Dylan Hanson.

This library is to be used at the core of every application of mine. It covers a broad range of topics, including management, web interaction, and UI coordination. It's pretty great.

Dedicated to a man that I admire greatly, even if he isn't the most admirable in and of himself: @Libraryaddict