//
//  TimeUtils.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/13/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public class TimeUtils {
	
	public static let calender: Calendar = {
		var cal = Calendar(identifier: Calendar.Identifier.gregorian)
		return cal
	}()
	
	public static let calenderTimeZoned: Calendar = {
		var cal = Calendar(identifier: Calendar.Identifier.gregorian)
		cal.timeZone = TimeZone(abbreviation: "EST")!
		return cal
	}()
	
	public static func timeToDate(to: Date) -> (years: Int, days: Int, hours: Int, minutes: Int, seconds: Int) {
		let cur = Date()
		var components = TimeUtils.calender.dateComponents([.year, .day, .hour, .minute, .second], from: cur, to: to)
		
		let years = components.year!
		let days = components.day!
		let hours = components.hour!
		let minutes = components.minute!
		let seconds = components.second!
		
		return (years: years, days: days, hours: hours, minutes: minutes, seconds: seconds) //Add one to account for seconds difference that's ignored.
	}
	
	public static func getDayInRelation(_ date: Date, offset: Int) -> Date? {
		if let newDate = TimeUtils.calender.date(byAdding: .day, value: offset, to: date) {
			return newDate
		}
		return nil
	}

	public static func timeToString(years: Int, days: Int, hours: Int, min: Int, sec: Int, displayAmount: Int = 2) -> String {
		var vYears = abs(years)
		var vDays = abs(days)
		var vHours = abs(hours)
		var vMin = abs(min)
		var vSec = abs(sec)
		
		var elements: [String] = []
		
		while vSec >= 60 {
			vSec -= 60
			vMin += 1
		}
		
		while vMin >= 60 {
			vMin -= 60
			vHours += 1
		}
		
		while vHours >= 24 {
			vHours -= 24
			vDays += 1
		}
		
		while vDays >= 365 {
			vDays -= 365
			vYears += 1
		}
		
		let yS = "\(vYears)y"
		let dS = "\(vDays)d"
		let hS = "\(vHours)h"
		let mS = "\(vMin)m"
		let sS = "\(vSec)s"
		
		if vYears != 0 { elements.append(yS) }
		if vDays != 0 { elements.append(dS) }
		if vHours != 0 { elements.append(hS) }
		if vMin != 0 { elements.append(mS) }
		if vSec != 0 { elements.append(sS) }
		
		var assembled = ""
		for i in 0..<displayAmount {
			if i < elements.count {
				assembled += (elements[i] + " ")
			}
		}
		
		return "\(assembled.dropLast())"
	}
}
