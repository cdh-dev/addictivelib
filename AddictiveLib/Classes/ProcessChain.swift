//
//  ProcessChain.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/14/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public class ProcessChain {
	
	public var chain: [(ProcessChain) -> Void]
	
	private var success: (ProcessChain) -> Void = {_ in}
	private var failure: (ProcessChain) -> Void = {_ in}
	
	public private(set) var started = false
	
	private var storedData: [String: Any]
	
	public init(_ chain: [(ProcessChain) -> Void] = []) {
		self.chain = chain
		
		self.storedData = [:]
	}
	
	@discardableResult
	public func success(_ callback: @escaping (ProcessChain) -> Void) -> ProcessChain {
		self.success = callback
		return self
	}
	
	@discardableResult
	public func failure(_ callback: @escaping (ProcessChain) -> Void) -> ProcessChain {
		self.failure = callback
		return self
	}
	
	@discardableResult
	public func link(_ link: @escaping (ProcessChain) -> Void) -> ProcessChain {
		self.chain.append(link)
		return self
	}
	
	public func hasData(_ key: String) -> Bool {
		return self.storedData[key] != nil
	}
	
	public func getData<Result>(_ key: String) -> Result? {
		guard let result = self.storedData[key] as? Result else {
			return nil
		}
		return result
	}
	
	@discardableResult
	public func setData(_ key: String, data: Any) -> ProcessChain {
		self.storedData[key] = data
		return self
	}
	
	public func start() {
		if !self.started
		{
			self.started = true
			self.next()
		}
	}
	
	public func next(_ success: Bool = true) {
		if !success {
			self.failure(self)
			return
		}
		
		if !self.chain.isEmpty {
			let link = self.chain.removeFirst()
			link(self)
		} else {
			self.success(self)
		}
	}
	
}
