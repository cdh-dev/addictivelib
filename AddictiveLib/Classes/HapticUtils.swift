//
//  HapticUtils.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/14/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit

public class HapticUtils {
	
	public static let SELECTION = UISelectionFeedbackGenerator()
	public static let NOTIFICATION = UINotificationFeedbackGenerator()
	public static let IMPACT = UIImpactFeedbackGenerator()
	
}
