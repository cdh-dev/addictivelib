//
//  SpacerTableCell.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 6/2/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit

public class SpacerTableCell: TableCell {
	
	public var backgroundColor: UIColor = .clear
	
	init() {
		super.init("template-spacer", nib: "SpacerTableCell", bundle: "org.cocoapods.AddictiveLib")
		
		self.setSelectionStyle(.none)
		self.setCallback() {
			form, cell in
			
			if let spacerCell = cell as? UISpacerTableCell {
				spacerCell.backgroundColor = self.backgroundColor
				spacerCell.background.backgroundColor = self.backgroundColor
			}
		}
	}
			
	public func setBackgroundColor(_ color: UIColor) -> SpacerTableCell {
		self.backgroundColor = color
		return self
	}
}

class UISpacerTableCell: UITableViewCell {
	
	@IBOutlet var background: UIView!
	
}
