//
//  TableHandler.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public protocol TableHandlerDataSource {
	
	func buildCells(handler: TableHandler, layout: TableLayout)

}

open class TableHandler: NSObject, TableHandlerDataSource, UITableViewDelegate, UITableViewDataSource {
	
	public let view: UITableView
	var layout: TableLayout // Shouldn't be modified outside the designated methods.
	
	public var dataSource: TableHandlerDataSource?
	
	private var registeredNibs: [String] = []
	
	public init(table: UITableView) {
		self.view = table
		self.layout = TableLayout()
		
		self.registeredNibs = []
		
		super.init()
		
		self.view.dataSource = self
		self.view.delegate = self
	}
	
	public func reload() {
		self.layout = TableLayout()
		(self.dataSource ?? self).buildCells(handler: self, layout: self.layout) // Call self if there isn't a dataSource provided
		
		self.layout.compile() // Compiles into Section form.
		self.view.reloadData()
	}
	
	final public func buildCells(handler: TableHandler, layout: TableLayout) {
		self.buildCells(layout: layout)
	}
	
	open func buildCells(layout: TableLayout) {
//		Slightly prettier overridable method.
	}
	
	open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if let section = self.layout.getSection(section) {
			return section.title
		}
		return nil
	}
	
	open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if let section = self.layout.getSection(section) {
			let headerView = UIView()
			headerView.backgroundColor = section.headerColor
			
			let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
			headerLabel.font = section.headerFont
			headerLabel.textColor = section.headerTextColor
			headerLabel.text = section.title
			
			headerLabel.sizeToFit()
			headerView.addSubview(headerLabel)
			
			headerLabel.translatesAutoresizingMaskIntoConstraints = false
			headerLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
			NSLayoutConstraint(item: headerLabel, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1.0, constant: section.headerIndent ?? 0).isActive = true
			
			return headerView
		}

		return nil
	}
	
	open func numberOfSections(in tableView: UITableView) -> Int {
		return self.layout.sectionCount
	}
	
	open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let section = self.layout.getSection(section) {
			return section.cellCount
		}
		return 0
	}
	
	open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let templateCell = self.layout.getCell(indexPath) else {
			return UITableViewCell()
		}
		
		if let nibName = templateCell.nibName {
			if !self.registeredNibs.contains(nibName) { // Nib hasn't already been registered with the table
				let nib = UINib.init(nibName: templateCell.nibName!, bundle: templateCell.nibBundle)
				self.view.register(nib, forCellReuseIdentifier: templateCell.reuseIdentifier)
				
				self.registeredNibs.append(nibName)
			}
		}
		
		let cell = self.view.dequeueReusableCell(withIdentifier: templateCell.reuseIdentifier)!
		templateCell.callback(templateCell, cell)
		
		if templateCell.estimatedHeight == nil {
			if let background = cell.backgroundView {
				templateCell.setEstimatedHeight(background.frame.height)
			}
		}
		
		if templateCell.selectionStyle != nil {
			cell.selectionStyle = templateCell.selectionStyle!
		}
		return cell
	}
	
	open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if let section = self.layout.getSection(indexPath), let cell = section.getCell(indexPath) {
			return cell.height
		}
		return self.view.rowHeight
	}
	
	open func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		if let section = self.layout.getSection(indexPath), let cell = section.getCell(indexPath) {
			return cell.getEstimatedHeight()
		}
		return self.view.estimatedRowHeight
	}
	
	open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if let section = self.layout.getSection(section), let height = section.headerHeight {
			return CGFloat(height)
		}
		return 0
	}
	
	open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		if let section = self.layout.getSection(section), let height = section.footerHeight {
			return CGFloat(height)
		}
		return 0
	}
	
	open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let cell = self.layout.getCell(indexPath) else {
			return
		}
		
		if cell.deselectOnSelection {
			self.view.deselectRow(at: indexPath, animated: true)
		}
		
		cell.selected(cell, indexPath)
	}
	
}
