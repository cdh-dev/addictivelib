//
//  Globals.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/14/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public class Globals {
	
	public static var DeviceID: String?
	
	public static var StorageID: String?
	public static var BundleID: String?
	
	private static var data: [String: Any] = [:]
	private static var urlBases: [String: String] = [:]
	
	public static func setData(_ key: String, data: Any?) {
		self.data[key] = data
	}
	
	public static func getData<Result>(_ key: String) -> Result? {
		guard let result = self.data[key] as? Result else {
			return nil
		}
		return result
	}
	
	public static func storeUrlBase(url: String) {
		self.storeUrlBase("global", url: url)
	}
	
	public static func getUrlBase() -> String {
		return self.getUrlBase("global") ?? ""
	}
	
	public static func storeUrlBase(_ key: String, url: String) {
		self.urlBases[key] = url
	}
	
	public static func getUrlBase(_ key: String) -> String? {
		return self.urlBases[key]
	}
	
}
