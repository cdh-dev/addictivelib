//
//  TableSection.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit

public class TableSection {
	
	private(set) public var cells: [TableCell]
	
	private(set) public var title: String?
	
	private(set) public var headerHeight: Int?
	private(set) public var headerColor: UIColor?
	private(set) public var headerFont: UIFont?
	private(set) public var headerTextColor: UIColor?
	private(set) public var headerIndent: CGFloat?
	
	private(set) public var footerHeight: Int?
	private(set) public var footerColor: UIColor?
	
	public var cellCount: Int {
		return self.cells.count
	}
	
	public init() {
		self.cells = []
	}
	
	public func getCell(_ path: IndexPath) -> TableCell? {
		return self.getCell(path.row)
	}
	
	public func getCell(_ row: Int) -> TableCell? {
		if row >= self.cells.count { return nil }
		return self.cells[row]
	}
	
	@discardableResult
	public func addCell(_ cell: TableCell) -> TableCell {
		self.cells.append(cell)
		return cell
	}
	
	@discardableResult
	public func addCell(_ cell: TableCell, at: Int) -> TableCell {
		self.cells.insert(cell, at: max(min(self.cells.count, at), 0))
		return cell
	}
	
	@discardableResult
	public func addCell(_ key: String) -> TableCell {
		return self.addCell(TableCell(key))
	}
	
	@discardableResult
	public func addCell(_ key: String, at: Int) -> TableCell {
		return self.addCell(TableCell(key), at: at)
	}
	
	@discardableResult
	public func addSpacerCell() -> SpacerTableCell {
		let cell = SpacerTableCell()
		self.addCell(cell)
		return cell
	}
	
	@discardableResult
	public func addDividerCell(left: Int, right: Int, backgroundColor: UIColor, insetColor: UIColor) -> DividerTableCell {
		let cell = DividerTableCell()
		self.addCell(cell)
		
		cell.leftMargin = CGFloat(left)
		cell.rightMargin = CGFloat(right)
		
		cell.backgroundColor = backgroundColor
		cell.dividerColor = insetColor
		
		return cell
	}
	
	@discardableResult
	public func setTitle(_ title: String?) -> TableSection {
		self.title = title
		return self
	}
	
	@discardableResult
	public func setHeaderHeight(_ height: Int?) -> TableSection {
		self.headerHeight = height
		return self
	}
	
	@discardableResult
	public func setHeaderColor(_ color: UIColor?) -> TableSection {
		self.headerColor = color
		return self
	}
	
	@discardableResult
	public func setHeaderFont(_ font: UIFont?) -> TableSection {
		self.headerFont = font
		return self
	}
	
	@discardableResult
	public func setHeaderTextColor(_ color: UIColor?) -> TableSection {
		self.headerTextColor = color
		return self
	}
	
	@discardableResult
	public func setHeaderIndent(_ indent: CGFloat?) -> TableSection {
		self.headerIndent = indent
		return self
	}
	
	@discardableResult
	public func setFooterHeight(_ height: Int?) -> TableSection {
		self.footerHeight = height
		return self
	}
	
	@discardableResult
	public func setFooterColor(_ color: UIColor?) -> TableSection {
		self.footerColor = color
		return self
	}
	
}
