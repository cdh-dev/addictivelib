//
//  DividerTableCell.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 6/2/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit

public class DividerTableCell: TableCell {
	
	public var leftMargin: CGFloat = 15.0
	public var rightMargin: CGFloat = 15.0
	
	public var backgroundColor: UIColor?
	public var dividerColor: UIColor = UIColor.gray
	
	init() {
		super.init("template-divider", nib: "DividerTableCell", bundle: "org.cocoapods.AddictiveLib")
		
		self.setSelectionStyle(.none)
		self.setHeight(1.0)
		self.setCallback() {
			form, cell in
			
			cell.backgroundColor = UIColor.clear
			
			if let dividerCell = cell as? UIDividerTableCell {
				dividerCell.leftConstraint.constant = self.leftMargin
				dividerCell.rightConstraint.constant = self.rightMargin
				
				dividerCell.background.backgroundColor = self.backgroundColor
				dividerCell.dividerBody.backgroundColor = self.dividerColor
			}
		}
	}
	
	public func setLeftMargin(_ margin: CGFloat) -> DividerTableCell {
		self.leftMargin = margin
		return self
	}
	
	public func setRightMargin(_ margin: CGFloat) -> DividerTableCell {
		self.rightMargin = margin
		return self
	}
	
	public func setBackgroundColor(_ color: UIColor) -> DividerTableCell {
		self.backgroundColor = color
		return self
	}
	
	public func setDividerColor(_ color: UIColor) -> DividerTableCell {
		self.dividerColor = color
		return self
	}
}

class UIDividerTableCell: UITableViewCell {
	
	@IBOutlet var background: UIView!
	@IBOutlet var dividerBody: UIView!
	
	@IBOutlet var leftConstraint: NSLayoutConstraint!
	@IBOutlet var rightConstraint: NSLayoutConstraint!
	
}
