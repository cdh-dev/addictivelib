//
//  TableLayout.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public class TableLayout {
	
	var components: [TableComponent] = [] // Describe the table inline.
	
	private var compiled = false
	private(set) public var sections: [TableSection] = [] // Is populated after generation of builders through their build method. Shouldn't be populated manually.
	public var sectionCount: Int { return self.sections.count }
	
	public init() { }
	
	func compile() {
		if self.compiled { return }
		
		for component in self.components {
			self.sections.append(contentsOf: component.compile())
		}
		
		self.compiled = true
	}
	
	public func getCell(_ path: IndexPath) -> TableCell? {
		if let section = self.getSection(path), let cell = section.getCell(path) {
			return cell
		}
		return nil
	}
	
	public func getSection(_ path: IndexPath) -> TableSection? {
		return self.getSection(path.section)
	}
	
	public func getSection(_ section: Int) -> TableSection? {
		if section >= self.sections.count { return nil }
		return self.sections[section]
	}
	
	public final func addSection() -> TableSection {
		let component = TableSectionComponent()
		self.components.append(component)
		return component.section
	}
	
	public final func addSection(_ section: TableSection) {
		let component = TableSectionComponent(section: section)
		self.components.append(component)
	}
	
	public final func addModule(_ module: TableModule) {
		self.components.append(module)
	}
	
}

// A wrapper that lets TableSections and TableModules to be added inline, and allows for nesting.
protocol TableComponent {
	
	func compile() -> [TableSection]
	
}

// A wrapper for the TableSection class. This is basically the most elemental form of a TableComponent.
class TableSectionComponent: TableComponent {
	
	let section: TableSection
	
	init(section: TableSection? = nil) {
		self.section = section ?? TableSection()
	}
	
	func compile() -> [TableSection] {
		return [self.section]
	}
	
}

// Used to add related collections of Sections.
open class TableModule: TableComponent {
	
	var children: [TableComponent] = [] // This will be user populated.
	
	public init() { }
	
	func compile() -> [TableSection] {
		self.build()
		
		var sections: [TableSection] = []
		self.children.forEach({ sections.append(contentsOf: $0.compile()) })
		
		return sections
	}
	
	open func build() {  } // Override point
	
	final public func addSection() -> TableSection {
		let component = TableSectionComponent()
		self.children.append(component)
		return component.section
	}
	
	final public func addSection(_ section: TableSection) {
		let component = TableSectionComponent(section: section)
		self.children.append(component)
	}
	
	final public func addModule(_ module: TableModule) {
		self.children.append(module)
	}
	
}
