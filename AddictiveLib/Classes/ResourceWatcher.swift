//
//  ResourceWatcher.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/14/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

open class ResourceWatcher<Payload> {
	
	private var success: [Int: (Payload) -> Void] = [:]
	private var failure: [Int: (Error) -> Void] = [:]
	
	public init() {
		
	}
	
	public func onSuccess(_ instance: AnyObject, _ callback: @escaping (Payload) -> Void) {
		self.success[ObjectIdentifier(instance).hashValue] = callback
	}
	
	public func onFailure(_ instance: AnyObject, _ callback: @escaping (Error) -> Void) {
		self.failure[ObjectIdentifier(instance).hashValue] = callback
	}
	
	public func unregisterSuccess(_ instance: AnyObject) {
		self.success.removeValue(forKey: ObjectIdentifier(instance).hashValue)
	}
	
	public func unregisterFailure(_ instance: AnyObject) {
		self.failure.removeValue(forKey: ObjectIdentifier(instance).hashValue)
	}
	
	public func handle(_ error: Error?, _ data: Payload?) {
		if error != nil || data == nil {
			let error = error ?? ResourceNullError(resource: "Payload")
			for callback in self.failure.values {
				callback(error)
			}
		} else {
			for callback in self.success.values {
				callback(data!)
			}
		}
	}
	
}

class ResourceNullError: Error {
	
	let resource: String
	var localizedDescription: String {
		return "The resource: \(resource) was null! Oops!"
	}
	
	init(resource: String) {
		self.resource = resource
	}
	
}
