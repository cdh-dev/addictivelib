//
//  StorageHub.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/13/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public class StorageHub {
	
	static public let instance = StorageHub()
	private let defaults = UserDefaults(suiteName: Globals.StorageID)!
	
	public func savePrefs(_ handler: StorageHandler) {
		self.defaults.set(handler.saveData(), forKey: handler.storageKey)
	}
	
	public func loadPrefs(_ handler: StorageHandler) {
		if let object = self.defaults.object(forKey: handler.storageKey) {
			handler.loadData(data: object)
		} else {
			handler.loadDefaults()
		}
	}
	
}

public protocol StorageHandler {
	
	var storageKey: String { get }
	
	func saveData() -> Any?
	func loadData(data: Any)
	func loadDefaults()
	
}
