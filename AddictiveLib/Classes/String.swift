//
//  String.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/13/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

public extension String {
	
	public func substring(start: Int, distance: Int) -> String
	{
		let startIndex = self.index(self.startIndex, offsetBy: start)
		let endIndex = self.index(startIndex, offsetBy: distance)
		
		return String(self[startIndex..<endIndex])
	}
	
}
