//
//  WebCall.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/14/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import Alamofire
import Unbox
import UnboxedAlamofire

open class UnboxWebCall<Payload: WebCallPayload, Result>: WebCall<Result> {
	
	override open func performRequest(request: DataRequest) {
		request.responseObject() {
			(response: DataResponse<Payload>) in
			
			if let error = self.validateResponse(dataResponse: response) {
				self.error(error: error)
				return
			}
			
			guard let payload = response.result.value else {
				self.error(error: ResourceNullError(resource: "Unbox Payload"))
				return
			}
			
			guard let result = self.convertToken(payload) else {
				self.error(error: ResourceNullError(resource: "Unboxed Result Token"))
				return
			}
			
			self.success(result: result)
		}
	}
	
	open func convertToken(_ data: Payload) -> Result? {
		// Override point
		return nil
	}
	
	final override public func convertData(_ data: Data) -> Result? {
		return nil
	}
	
}

public protocol WebCallPayload: Unboxable {
	
}
