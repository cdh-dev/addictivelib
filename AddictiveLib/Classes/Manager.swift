//
//  Manager.swift
//  AddictiveLib
//
//  Created by Dylan Hanson on 4/13/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

open class Manager {
	
	public let name: String
	public private(set) var storage: [StorageHandler]
	
	public init(_ name: String) {
		self.name = name
		self.storage = []
	}
	
	public func out(_ msg: String) {
		print("\(self.name): \(msg)")
	}
	
	public func registerStorage(_ storageHandler: StorageHandler, load: Bool = true) {
		self.storage.append(storageHandler)
		if load { self.loadStorage(storageHandler) }
	}
	
	public func loadStorage(_ storageHandler: StorageHandler) {
		StorageHub.instance.loadPrefs(storageHandler)
	}
	
	public func loadStorage() {
		for handler in self.storage {
			StorageHub.instance.loadPrefs(handler)
		}
	}
	
	public func saveStorage(_ storageHandler: StorageHandler) {
		StorageHub.instance.savePrefs(storageHandler)
	}
	
	public func saveStorage() {
		for handler in self.storage {
			StorageHub.instance.savePrefs(handler)
		}
	}
	
}
